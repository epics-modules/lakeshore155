####################################################################################
## Description: This is a database for the EPICS IOC of LakeShore 155              #
##                                                                                 #
## Database definition for basic SCPI (IEEE488.2) commands                         #
## copyright:   (C) 2015-2020 European Spallation Source ERIC                      #
## version:     0.0.1                                                              #
## date:        2020/DECEMBER                                                      #
## author:      Tamas Kerenyi                                                      #
## email:       tamas.kerenyi@ess.eu                                               #
## ------------------------------------------------------------------------------- #
## version:     0.0.4                                                              #
## date:        2021/FEBRUARY                                                      #
## author:      Douglas Bezerra Beniz                                              #
## email:       douglas.bezerra.beniz@ess.eu                                       #
####################################################################################

# -----------------------------------------------------------------------------
# COMMON to both VOLTAGE and CURRENT
# -----------------------------------------------------------------------------
#
record(stringin, "$(P)$(R)IDN-R") {
    field(DESC, "SCPI identification string")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getIDN() $(PORT) $(A)")
    field(PINI, "YES")
}

record(bo, "$(P)$(R)OutputState-S") {
    field(DESC, "Set Output Channel On/Off")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setD(OUTP:STAT) $(PORT) $(A)")
    field(ZNAM, "OFF")
    field(ONAM, "ON")
}

record(bi, "$(P)$(R)OutputState-RBV") {
    field(DESC, "Get Output Channel On/Off RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getD(OUTP:STAT) $(PORT) $(A)")
    field(PINI, "YES")
    field(ONAM, "ON")
    field(ZNAM, "OFF")
    field(SCAN, "1 second")
    field(FLNK, "$(P)$(R)Terminals-RBV")
}

record(mbbo, "$(P)$(R)Terminals-S") {
    field(DESC, "Set terminals rear/front")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setS_Enum(ROUT:TERM,REAR|FRONT) $(PORT) $(A)")
    field(ZRVL, "0")
    field(ZRST, "Rear")
    field(ONVL, "1")
    field(ONST, "Front")
}

record(mbbi, "$(P)$(R)Terminals-RBV") {
    field(DESC, "Get terminals rear/front RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getS_Enum(ROUT:TERM,REAR|FRONT) $(PORT) $(A)")
    field(PINI, "YES")
    field(ZRVL, "0")
    field(ZRST, "Rear")
    field(ONVL, "1")
    field(ONST, "Front")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)FunctionMode-RBV")
}

record(mbbo, "$(P)$(R)FunctionMode-S") {
    field(DESC, "Set source function mode")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setS_Enum(SOUR:FUNC:MODE,VOLTAGE|CURRENT) $(PORT) $(A)")
    field(ZRVL, "0")
    field(ZRST, "Voltage")
    field(ONVL, "1")
    field(ONST, "Current")
}

record(mbbi, "$(P)$(R)FunctionMode-RBV") {
    field(DESC, "Get source function mode")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getS_Enum(SOUR:FUNC:MODE,VOLTAGE|CURRENT) $(PORT) $(A)")
    field(ZRVL, "0")
    field(ZRST, "Voltage")
    field(ONVL, "1")
    field(ONST, "Current")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)FunctionShape-RBV")
}

record(mbbo, "$(P)$(R)FunctionShape-S") {
    field(DESC, "Set source function shape")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setS_Enum(SOUR:FUNC:SHAPE,DC|SINUSOID) $(PORT) $(A)")
    field(ZRVL, "0")
    field(ZRST, "DC")
    field(ONVL, "1")
    field(ONST, "AC (Sinusoid)")
}

record(mbbi, "$(P)$(R)FunctionShape-RBV") {
    field(DESC, "Get source function shape")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getS_Enum(SOUR:FUNC:SHAPE,DC|SINUSOID) $(PORT) $(A)")
    field(ZRVL, "0")
    field(ZRST, "DC")
    field(ONVL, "1")
    field(ONST, "AC (Sinusoid)")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)Frequency-RBV")
}

record(ao, "$(P)$(R)Frequency-S") {
    field(DESC, "Set source frequency")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setF(SOUR:FREQ) $(PORT) $(A)")
    field(EGU,  "Hz")
}

record(ai, "$(P)$(R)Frequency-RBV") {
    field(DESC, "Get source frequency")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getF(SOUR:FREQ) $(PORT) $(A)")
    field(EGU,  "Hz")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)FrequencyPhase-RBV")
}

record(ao, "$(P)$(R)FrequencyPhase-S") {
    field(DESC, "Set source frequency phase adjust")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setF(SOUR:PHAS:ADJ) $(PORT) $(A)")
    field(EGU,  ".o")
    field(DRVH, "180")
    field(DRVL, "-180")
}

record(ai, "$(P)$(R)FrequencyPhase-RBV") {
    field(DESC, "Get source frequency phase adjust")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getF(SOUR:PHAS:ADJ) $(PORT) $(A)")
    field(EGU,  ".o")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)VoltAmpRaw-R")
}

# -----------------------------------------------------------------------------
# VOLTAGE :: SOUR:VOLT
# -----------------------------------------------------------------------------
#
record(mbbo, "$(P)$(R)VoltEgu-S") {
    field(DESC, "Select Range")
    field(DTYP, "Raw Soft Channel")
    field(ZRVL, "1")
    field(ZRST, "V")
    field(ONVL, "1000")
    field(ONST, "mV")
    field(TWVL, "1000000")
    field(TWST, "uV")
    field(OUT,  "$(P)$(R)__VoltDivValue PP")
}

record(ao, "$(P)$(R)__VoltDivValue") {
    field(DTYP,"Raw Soft Channel")
    field(DESC,"Div Value")
}

record(ao, "$(P)$(R)VoltValue-S") {
    field(DTYP,"Raw Soft Channel")
    field(DESC,"Voltage Value set on the GUI")
}

record(calcout, "$(P)$(R)__CalcVolt"){
    field(DESC, "Voltage Calculation")
    field(INPA, "$(P)$(R)__VoltDivValue CPP")
    field(INPB, "$(P)$(R)VoltValue-S CPP")
    field(OOPT, "On Change")
    field(CALC, "B/A")
    field(OUT,  "$(P)$(R)VoltAmp-S PP")
}

record(ao, "$(P)$(R)VoltAmp-S") {
    field(DESC, "Set source voltage amplitude")
    field(SDIS, "$(P)$(R)__VAmpValSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setF(SOUR:VOLT:LEV:IMM:AMPL) $(PORT) $(A)")
    field(EGU,  "V")
}

record(ai, "$(P)$(R)VoltAmp-R") {
    field(DESC, "Get source voltage amplitude")
    field(DTYP, "Soft Channel")
}

record(ai, "$(P)$(R)VoltAmpRaw-R") {
    field(DESC, "Get source voltage amplitude")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getF(SOUR:VOLT:LEV:IMM:AMPL) $(PORT) $(A)")
    field(EGU,  "V")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)VoltDCOffset-RBV")
}

record(calcout, "$(P)$(R)__VAmpValCalc"){
    field(DESC, "Volt amplitude adjusted")
    field(INPA, "$(P)$(R)VoltAmpRaw-R CP")
    field(OOPT, "On Change")
    field(CALC, "A>=1?A:(A>=1e-03?A*1e03:A*1e06)")
    field(OUT,  "$(P)$(R)VoltAmp-R PP")
}

record(scalcout, "$(P)$(R)__VAmpEguCalc") {
    field(DESC, "Volt amplitude RBV EGU")
    field(INPA, "$(P)$(R)VoltAmpRaw-R CP")
    field(AA,   "V")
    field(BB,   "mV")
    field(CC,   "uV")
    field(CALC, "A>=1?AA:(A>=1e-03?BB:CC)")
    field(OUT,  "$(P)$(R)VoltAmp-R.EGU PP")
}

record(ao, "$(P)$(R)__VAmpValSync") {
    field(DESC, "Sync voltage amplitude")
    field(DTYP, "Soft Channel")
    field(DOL,  "$(P)$(R)VoltAmp-R CP")
    field(OMSL, "closed_loop")
    field(OUT,  "$(P)$(R)VoltValue-S PP")
    field(FLNK, "$(P)$(R)__VAmpEguSync")
}

record(calcout, "$(P)$(R)__VAmpEguSync") {
    field(DESC, "Sync voltage amplitude EGU")
    field(INPA, "$(P)$(R)VoltAmpRaw-R")
    field(CALC, "A>=1?0:(A>=1e-03?1:2)")
    field(OUT,  "$(P)$(R)VoltEgu-S PP")
}

record(ao, "$(P)$(R)VoltDCOffset-S") {
    field(DESC, "Set source voltage offset")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setF(SOUR:VOLT:LEV:IMM:OFFS) $(PORT) $(A)")
    field(EGU,  "V")
}

record(ai, "$(P)$(R)VoltDCOffset-RBV") {
    field(DESC, "Get source voltage offset")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getF(SOUR:VOLT:LEV:IMM:OFFS) $(PORT) $(A)")
    field(EGU,  "V")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)VoltRangeAuto-RBV")
}

record(bo, "$(P)$(R)VoltRangeAuto-S") {
    field(DESC, "Input Voltage Range Auto")
    field(DTYP, "Soft Channel")
    field(OUT,  "$(P)$(R)__VoltRangeAutoSet PP")
    field(ZNAM, "OFF")
    field(ONAM, "ON")
}

record(bo, "$(P)$(R)__VoltRangeAutoSet") {
    field(DESC, "Set Voltage Range Auto On/Off")
    field(SDIS, "$(P)$(R)__VoltRangeAutoSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setD(SOUR:VOLT:RANG:AUTO) $(PORT) $(A)")
    field(ZNAM, "OFF")
    field(ONAM, "ON")
}

record(bi, "$(P)$(R)VoltRangeAuto-RBV") {
    field(DESC, "Get Voltage Range Auto RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getD(SOUR:VOLT:RANG:AUTO) $(PORT) $(A)")
    field(PINI, "YES")
    field(ONAM, "ON")
    field(ZNAM, "OFF")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)VoltRange-RBV")
}

record(bo, "$(P)$(R)__VoltRangeAutoSync") {
    field(DESC, "Sync Voltage Range Auto")
    field(DTYP, "Soft Channel")
    field(DOL,  "$(P)$(R)VoltRangeAuto-RBV CP")
    field(OMSL, "closed_loop")
    field(OUT,  "$(P)$(R)VoltRangeAuto-S PP")
}

record(mbbo, "$(P)$(R)VoltRange-S") {
    field(DESC, "Set Voltage Range")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setF_Enum(SOUR:VOLT:RANG,100|10|1|0.1|0.01) $(PORT) $(A)")
    field(ZRVL, "0")
    field(ZRST, "100 V")
    field(ONVL, "1")
    field(ONST, "10 V")
    field(TWVL, "2")
    field(TWST, "1 V")
    field(THVL, "3")
    field(THST, "100 mV")
    field(FRVL, "4")
    field(FRST, "10 mV")
}

record(mbbi, "$(P)$(R)VoltRange-RBV") {
    field(DESC, "Get Voltage Range RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getF_Enum(SOUR:VOLT:RANG,100|10|1|0.1|0.01) $(PORT) $(A)")
    field(ZRVL, "0")
    field(ZRST, "100 V")
    field(ONVL, "1")
    field(ONST, "10 V")
    field(TWVL, "2")
    field(TWST, "1 V")
    field(THVL, "3")
    field(THST, "100 mV")
    field(FRVL, "4")
    field(FRST, "10 mV")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)VoltLimit-RBV")
}

record(ao, "$(P)$(R)VoltLimit-S") {
    field(DESC, "Set source voltage limit")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setF(SOUR:VOLT:LIM) $(PORT) $(A)")
    field(EGU,  "V")
    field(DRVH, "100")
    field(DRVL, "0")
}

record(ai, "$(P)$(R)VoltLimit-RBV") {
    field(DESC, "Get source voltage limit RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getF(SOUR:VOLT:LIM) $(PORT) $(A)")
    field(EGU,  "V")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)CurrLimitProt-RBV")
}

record(ao, "$(P)$(R)CurrLimitProt-S") {
    field(DESC, "Set source current limit")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setF(SOUR:VOLT:SENS:CURR:DC:PROT:LEV) $(PORT) $(A)")
    field(EGU,  "A")
    field(DRVH, 0.1)
    field(DRVL, 1e-07)
}

record(ai, "$(P)$(R)CurrLimitProt-RBV") {
    field(DESC, "Get source current limit RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getF(SOUR:VOLT:SENS:CURR:DC:PROT:LEV) $(PORT) $(A)")
    field(EGU,  "A")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)CurrLimitTrip-RBV")
}

record(bo, "$(P)$(R)CurrLimitTrip-S") {
    field(DESC, "Set Current Limit Tripped")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setD(SOUR:VOLT:SENS:CURR:DC:PROT:TRIP) $(PORT) $(A)")
    field(ZNAM, "OFF")
    field(ONAM, "ON")
}

record(bi, "$(P)$(R)CurrLimitTrip-RBV") {
    field(DESC, "Get Current Limit Tripped RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getD(SOUR:VOLT:SENS:CURR:DC:PROT:TRIP) $(PORT) $(A)")
    field(PINI, "YES")
    field(ONAM, "ON")
    field(ZNAM, "OFF")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)CurrAmpRaw-R")
}


# -----------------------------------------------------------------------------
# CURRENT :: SOUR:CURR
# -----------------------------------------------------------------------------
#
record(mbbo, "$(P)$(R)CurrEgu-S") {
    field(DESC, "Select Range")
    field(DTYP, "Raw Soft Channel")
    field(ZRVL, "1")
    field(ZRST, "A")
    field(ONVL, "1000")
    field(ONST, "mA")
    field(TWVL, "1000000")
    field(TWST, "uA")
    field(OUT,  "$(P)$(R)__CurrDivValue PP")
}

record(ao, "$(P)$(R)__CurrDivValue") {
    field(DTYP,"Raw Soft Channel")
    field(DESC,"Div Value")
}

record(ao, "$(P)$(R)CurrValue-S") {
    field(DTYP,"Raw Soft Channel")
    field(DESC,"Current Value set on the GUI")
}

record(calcout, "$(P)$(R)__CalcCurr"){
    field(DESC, "Current Calculation")
    field(INPA, "$(P)$(R)__CurrDivValue CPP")
    field(INPB, "$(P)$(R)CurrValue-S CPP")
    field(OOPT, "On Change")
    field(CALC, "B/A")
    field(OUT,  "$(P)$(R)CurrAmp-S PP")
}

record(ao, "$(P)$(R)CurrAmp-S") {
    field(DESC, "Set source current amplitude")
    field(SDIS, "$(P)$(R)__CAmpValSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setF(SOUR:CURR:LEV:IMM:AMPL) $(PORT) $(A)")
    field(EGU,  "A")
}

record(ai, "$(P)$(R)CurrAmp-R") {
    field(DESC, "Get source current ampl.")
    field(DTYP, "Soft Channel")
}

record(ai, "$(P)$(R)CurrAmpRaw-R") {
    field(DESC, "Get source current amplitude")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getF(SOUR:CURR:LEV:IMM:AMPL) $(PORT) $(A)")
    field(EGU,  "A")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)CurrDCOffset-RBV")
}

record(calcout, "$(P)$(R)__CAmpValCalc"){
    field(DESC, "Curr amplitude adjusted")
    field(INPA, "$(P)$(R)CurrAmpRaw-R CP")
    field(OOPT, "On Change")
    field(CALC, "A>=1?A:(A>=1e-03?A*1e03:A*1e06)")
    field(OUT,  "$(P)$(R)CurrAmp-R PP")
}

record(scalcout, "$(P)$(R)__CAmpEguCalc") {
    field(DESC, "Curr amplitude RBV EGU")
    field(INPA, "$(P)$(R)CurrAmpRaw-R CP")
    field(AA,   "A")
    field(BB,   "mA")
    field(CC,   "uA")
    field(CALC, "A>=1?AA:(A>=1e-03?BB:CC)")
    field(OUT,  "$(P)$(R)CurrAmp-R.EGU PP")
}

record(ao, "$(P)$(R)__CAmpValSync") {
    field(DESC, "Sync current amplitude")
    field(DTYP, "Soft Channel")
    field(DOL,  "$(P)$(R)CurrAmp-R CP")
    field(OMSL, "closed_loop")
    field(OUT,  "$(P)$(R)CurrValue-S PP")
    field(FLNK, "$(P)$(R)__CAmpEguSync")
}

record(calcout, "$(P)$(R)__CAmpEguSync") {
    field(DESC, "Sync current amplitude EGU")
    field(INPA, "$(P)$(R)CurrAmpRaw-R")
    field(CALC, "A>=1?0:(A>=1e-03?1:2)")
    field(OUT,  "$(P)$(R)CurrEgu-S PP")
}

record(ao, "$(P)$(R)CurrDCOffset-S") {
    field(DESC, "Set source current offset")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setF(SOUR:CURR:LEV:IMM:OFFS) $(PORT) $(A)")
    field(EGU,  "A")
}

record(ai, "$(P)$(R)CurrDCOffset-RBV") {
    field(DESC, "Get source current offset")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getF(SOUR:CURR:LEV:IMM:OFFS) $(PORT) $(A)")
    field(EGU,  "A")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)CurrRangeAuto-RBV")
}

record(bo, "$(P)$(R)CurrRangeAuto-S") {
    field(DESC, "Input Current Range Auto")
    field(DTYP, "Soft Channel")
    field(OUT,  "$(P)$(R)__CurrRangeAutoSet PP")
    field(ZNAM, "OFF")
    field(ONAM, "ON")
}

record(bo, "$(P)$(R)__CurrRangeAutoSet") {
    field(DESC, "Set Current Range Auto On/Off")
    field(SDIS, "$(P)$(R)__CurrRangeAutoSync.PACT")
    field(DISV, "1")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setD(SOUR:CURR:RANG:AUTO) $(PORT) $(A)")
    field(ZNAM, "OFF")
    field(ONAM, "ON")
}

record(bi, "$(P)$(R)CurrRangeAuto-RBV") {
    field(DESC, "Get Current Range Auto RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getD(SOUR:CURR:RANG:AUTO) $(PORT) $(A)")
    field(PINI, "YES")
    field(ONAM, "ON")
    field(ZNAM, "OFF")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)CurrRange-RBV")
}

record(bo, "$(P)$(R)__CurrRangeAutoSync") {
    field(DESC, "Sync Current Range Auto")
    field(DTYP, "Soft Channel")
    field(DOL,  "$(P)$(R)CurrRangeAuto-RBV CP")
    field(OMSL, "closed_loop")
    field(OUT,  "$(P)$(R)CurrRangeAuto-S PP")
}

record(mbbo, "$(P)$(R)CurrRange-S") {
    field(DESC, "Set Current Range")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setF_Enum(SOUR:CURR:RANG,1e-06|1e-05|0.0001|0.001|0.01|0.1) $(PORT) $(A)")
    field(ZRVL, "0")
    field(ZRST, "1 uA")
    field(ONVL, "1")
    field(ONST, "10 uA")
    field(TWVL, "2")
    field(TWST, "100 uA")
    field(THVL, "3")
    field(THST, "1 mA")
    field(FRVL, "4")
    field(FRST, "10 mA")
    field(FVVL, "5")
    field(FVST, "100 mA")
}

record(mbbi, "$(P)$(R)CurrRange-RBV") {
    field(DESC, "Get Current Range RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getF_Enum(SOUR:CURR:RANG,1e-06|1e-05|0.0001|0.001|0.01|0.1) $(PORT) $(A)")
    field(ZRVL, "0")
    field(ZRST, "1 uA")
    field(ONVL, "1")
    field(ONST, "10 uA")
    field(TWVL, "2")
    field(TWST, "100 uA")
    field(THVL, "3")
    field(THST, "1 mA")
    field(FRVL, "4")
    field(FRST, "10 mA")
    field(FVVL, "5")
    field(FVST, "100 mA")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)CurrLimit-RBV")
}

record(ao, "$(P)$(R)CurrLimit-S") {
    field(DESC, "Set source current limit")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setF(SOUR:CURR:LIM) $(PORT) $(A)")
    field(EGU,  "A")
    field(DRVH, "0.1")
    field(DRVL, "0")
}

record(ai, "$(P)$(R)CurrLimit-RBV") {
    field(DESC, "Get source current limit RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getF(SOUR:CURR:LIM) $(PORT) $(A)")
    field(EGU,  "A")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)DCVoltComp-RBV")
}

record(ao, "$(P)$(R)DCVoltComp-S") {
    field(DESC, "Set DC voltage compliance")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setF(SOUR:CURR:SENS:VOLT:DC:PROT:LEV) $(PORT) $(A)")
    field(EGU,  "V")
    field(DRVH, "100")
    field(DRVL, "1")
}

record(ai, "$(P)$(R)DCVoltComp-RBV") {
    field(DESC, "Get DC voltage compliance RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getF(SOUR:CURR:SENS:VOLT:DC:PROT:LEV) $(PORT) $(A)")
    field(EGU,  "V")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)ACVoltComp-RBV")
}

record(mbbo, "$(P)$(R)ACVoltComp-S") {
    field(DESC, "Set AC voltage compliance")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setD(SOUR:CURR:SENS:VOLT:AC:VRANGE) $(PORT) $(A)")
    field(ZRVL, "10")
    field(ZRST, "10 V")
    field(ONVL, "100")
    field(ONST, "100 V")
}

record(mbbi, "$(P)$(R)ACVoltComp-RBV") {
    field(DESC, "Get AC voltage compliance RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getD(SOUR:CURR:SENS:VOLT:AC:VRANGE) $(PORT) $(A)")
    field(ZRVL, "10")
    field(ZRST, "10 V")
    field(ONVL, "100")
    field(ONST, "100 V")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)CompVoltAnnnun-RBV")
}

record(bo, "$(P)$(R)CompVoltAnnnun-S") {
    field(DESC, "Set Compliance Voltage annunciator")
    field(DTYP, "stream")
    field(OUT,  "@lakeshore155.proto setD(SOUR:CURR:SENS:VOLT:DC:PROT:TRIP) $(PORT) $(A)")
    field(ZNAM, "OFF")
    field(ONAM, "ON")
}

record(bi, "$(P)$(R)CompVoltAnnnun-RBV") {
    field(DESC, "Get Compliance Voltage annunciator RBV")
    field(DTYP, "stream")
    field(INP,  "@lakeshore155.proto getD(SOUR:CURR:SENS:VOLT:DC:PROT:TRIP) $(PORT) $(A)")
    field(ZNAM, "OFF")
    field(ONAM, "ON")
    field(SCAN, "Passive")
}
